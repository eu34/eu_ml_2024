import  string
import random
import pandas as pd
import numpy as np

N = 5
id_length = 11
# print(symbols, len(symbols))
def get_id():
    symbols = string.ascii_uppercase + string.digits
    id = ""
    for i in range(id_length):
        r = random.randint(0, len(symbols)-1)
        # print(r)
        id += symbols[r]
    return (id)
def generate_date():
    id_s = set()
    while len(id_s) != N:
        id_s.add(get_id())
    # print(id_s)
    # print(len(id_s))
    data = {
        "id": list(id_s),
        "English": np.random.randint(0, 100, N),
        "Computer Skills": np.random.randint(0, 100, N),
        "Academic Writing": np.random.randint(0, 100, N)
    }
    df = pd.DataFrame(data)
    # print(df)
    df.to_excel("data.xlsx")
# generate_date()

def print_mean():
    df = pd.read_excel("data.xlsx")
    print(df)
    # print(df['English'].mean())
    # print(df['Computer Skills'].mean())
    # print(df['Academic Writing'].mean())
    print((df['English'] >= 51))
    print((df['English'] >= 51).sum())
    print((df['English']).sum())

print_mean()