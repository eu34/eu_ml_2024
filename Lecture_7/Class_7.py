import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt

#
# N = 5000
# Min = 100
# Max = 1000
# par = 50
# sal = np.random.randint(Min, Max, N)
# print(sal)
# lessMean = np.sum(sal<sal.mean())
# countPar = math.floor(N * par / 100)
# print(countPar)
# sortSal = np.sort(sal)
# print(sortSal)
#
# print(sortSal[countPar-1], sortSal[countPar])
# percentile = round(np.random.uniform(sortSal[countPar-1], sortSal[countPar]), 2)
# print(percentile)
# print(np.percentile(sal, 50))

# print(N)
# print(round(sal.mean(), 2))
# print(lessMean)
# print(f"{round(lessMean/N*100, 2)}%")

N = 10
sal_v =  [300, 450, 700, 850, 1700, 1950, 2000, 3500, 12000, 30000]
sal = np.random.choice(sal_v, N)
print(sal)



# data = {
#     "salary": sal
# }
#
# df =  pd.DataFrame(data)
# df.to_excel("salary.xlsx")

plt.hist(sal)
plt.show()