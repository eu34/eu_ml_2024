import pandas as pd
import pprint as pr
import  random

data = {
    "names": ["Giorgi", "Gio", "Kaxa"],
    "subjects": ["Machine Learning", "Java", "Linux"],
    "grades": [87, 52, 43]
}
# print(data)
df = pd.DataFrame(data)
# print(df)

# print(df.names)
df.to_excel("data.xlsx")

"""
    students.xlsx ფაილიდან ამოვიღოთ სტუდენტები სახელი და გვარები,
    informatics.xlsx ფაილიდან ამოვიღოთ სასწავლო კურსები,
    თითეული სტუდენტისთვის შემთხვევითად ავირჩიოთ ნებისმიერი 4 საგანი და ჩავწორეთ ექსელში.
"""

students = pd.read_excel("students.xlsx", usecols=["First name", "Surname"])
informatics = pd.read_excel("informatics.xlsx")

# print(informatics.columns)
l =  informatics["სასწავლო კომპონენტი"].tolist()
# print(l)


data = {
    "name": students['First name'].tolist(),
    "lastname": students['Surname'].tolist(),
    "subject 1": random.choices(l, k= len(students['First name'].tolist()))
}
# pr.pprint(data)

# df2 = pd.DataFrame(data)
# df2.to_excel("full_data.xlsx")

subjects = random.sample(l, k=5)
print(subjects)
