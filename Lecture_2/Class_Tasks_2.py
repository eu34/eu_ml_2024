# Tasks 2 _ N1
import math
def task2_1(a):
    print(round(a, 1))
    print(math.ceil(a*10)/10)
    print(math.floor(a*10)/10)
    print(math.trunc(a))
# task2_1(2.785)

# for i in range(2, 10, 3):
#     print(i)

l = [3, 4, 5, 5]
# for i in l:
#     print(i)

import random as rd
# for i  in range(10):
#     print(rd.randint(2, 100))

for i in range(10):
    print(round(rd.uniform(10, 20), 4))

