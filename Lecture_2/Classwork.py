# l = [3, 8, 9.8, "EU", "EuStudents"]
# print(l)
# for x in l:
#     print(x)
# l.append(76)
# print(l)
# l.insert(3, 78)
# print(l)

# t = (2, 4, 5, 8.9, 5, 2, "EU")
# print(t)
# print(t[2])
#
# t = t + (3, 5)
# print(t)
# t_l = list(t)
# t_l.append("New Element")
# n_t = tuple(t_l)
# print(n_t)

# s = {3, 3, 4, 8, 9.8, "Eu", "Students"}
# print(s)
# l = [3, 3, 3, 38, 38, 38]
# s1 = set(l)
# print(s1)
#
# s.add(5)
# print(s)
# s.pop()
# print(s)

# d = {'name':"Jeni", "GPA":4, "University":"European University"}
# print(d)
# print(d['name'])

# def f(a, b):
#     print(a+b)
#     return a*b
#
# print(f(3,4))

# x = lambda a, b : a * b
# print(x(5, 6))