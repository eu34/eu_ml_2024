import random
import numpy as n
import pandas as pd

N = 20
# print(random.random())
# print(random.randint(2, 10))
# print(random.sample(range(2, 7), 5))
# print(random.uniform(1, 4))
# print(n.random.randint(100, 999, N))
# print(n.round(n.random.uniform(1, 10, N), 2))
l = {
    'id': n.random.randint(100, 999, N),
    'day-1': n.round(n.random.uniform(1, 10, N), 2),
    'day-2': n.round(n.random.uniform(1, 10, N), 2),
    'day-3': n.round(n.random.uniform(1, 10, N), 2),
    'day-4': n.round(n.random.uniform(1, 10, N), 2),
    'day-5': n.round(n.random.uniform(1, 10, N), 2),
}
# print(l)
df = pd.DataFrame(l)
# print(df)
df.to_excel("data.xlsx")